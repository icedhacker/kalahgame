package com.backbase.kalah;

/**
 * An implementation of a 2 Player Kalah Game.
 */
public final class KalahGame {
	private final KalahPlayer player1;
	private final KalahPlayer player2;
	private GameState gameState;
	private final int stonesPerPit;

	/** 
	 * The different states of a Kalah game. 
	 */
	enum GameState {
		NOT_STARTED, PLAYER1, PLAYER2, GAME_OVER, ERROR
	}

	/**
	 * The different moves possible in the Kalah game.
	 */
	enum GameMove {
		ILLEGAL, CONTINUE, CAPTURE, PLAY_AGAIN
	}

	KalahGame(String name1, String name2) {
		player1 = new KalahPlayer(name1);
		player2 = new KalahPlayer(name2);
		gameState = GameState.NOT_STARTED;
		stonesPerPit = 6;
	}

	KalahGame(String name1, String name2, int pitCount, int stonesPerPit) {
		player1 = new KalahPlayer(name1, pitCount, stonesPerPit);
		player2 = new KalahPlayer(name2, pitCount, stonesPerPit);
		gameState = GameState.NOT_STARTED;
		this.stonesPerPit = stonesPerPit;
	}

	KalahPlayer getPlayer1() {
		return player1;
	}

	KalahPlayer getPlayer2() {
		return player2;
	}
	
	int getStonesPerPit() {
		return stonesPerPit;
	}

	/**
	 * @return the current game state.
	 */
	public GameState getGameState() {
		return gameState;
	}

	/**
	 * Starts the game if the game is not yet started, else sets the game state
	 * to error.
	 */
	public void startGame() {
		if (gameState.equals(GameState.NOT_STARTED)) {
			gameState = GameState.PLAYER1;
		} else {
			gameState = GameState.ERROR;
		}
	}

	/**
	 * Processes the players move, i.e. validates the move / picks from the
	 * input index and changes the game state for deciding the next move.
	 * 
	 * @param index
	 *            the index of the pit to pick stones.
	 */
	public void playMove(int index) {
		if (!gameState.equals(GameState.PLAYER1) && !gameState.equals(GameState.PLAYER2)) {
			gameState = GameState.ERROR;
			return;
		}
		GameMove nextMove = processPlayerMove(index);
		switch (nextMove) {
			case CONTINUE:
			case CAPTURE:
				gameState = changePlayer();
				break;
			case ILLEGAL:
				// TODO(sanath): Throw custom exception for this scenario.
				// For now, Play again or allow the player to make another move.
			case PLAY_AGAIN:
				break;
		}
		if (isGameOver()) {
			gameState = GameState.GAME_OVER;
		}
	}

	/**
	 * Get the winner of the game.
	 *
	 * Minimum score to finish a game with tie is the initial total number of
	 * stones in a single players side.
	 * For eg: For default case of 6 stones per pit in 6 pits per player.
	 * Score to Draw is 36 each.
	 * Score to Win is anything greater than 36.
	 *
	 * @return the winning player. If the game is a tie, return null.
	 */
	public KalahPlayer getWinner() {
		int minScoreToFinish = stonesPerPit * player1.getPitsCount();
		if (player1.getScore() > minScoreToFinish) {
			return player1;
		} else if (player2.getScore() > minScoreToFinish) {
			return player2;
		}
		return null;
	}

	/**
	 * Performs all the tasks related to a specific player move :
	 * 1. Validate the Move.
	 * 2. Pick All the stones from the selected pit.
	 * 3. Sows the stones into each of the following pits.
	 * 4. Checks for the edge cases and takes action.
	 * 5. Return a GameMove to help decide on the next move / GameState.
	 *
	 * @param index
	 *            the index of the pit to pick stones.
	 * @return the GameMove for deciding next move.
	 */
	GameMove processPlayerMove(int index) {
		if (!validPit(index)) {
			return GameMove.ILLEGAL;
		}
		KalahPlayer currentPlayer = getCurrentPlayer();
		GameMove nextMove = GameMove.CONTINUE;

		// Get the players selected pit.
		KalahPit pit = currentPlayer.getPit(index);

		// Pick all the stones in the selected pit.
		int stones = pit.pickStones();

		boolean isOppositePit = false;
		while (stones > 0) {
			// Reduce a stone from picked stones.
			stones--;
			index++;
			nextMove = GameMove.CONTINUE;

			pit = currentPlayer.getPit(index);
			if (pit != null) {
				if (!isOppositePit && pit.isEmpty()) {
					// Set the next move to Capture to capture 
					// opposite pit stones if this is the last stone.
					nextMove = GameMove.CAPTURE;
				}
				pit.sowStone();
			} else {
				if (!isOppositePit) {
					// Stone is added to the players kalah / score.
					currentPlayer.addScore(1);
					// Set next move to play again if this is the last stone.
					nextMove = GameMove.PLAY_AGAIN;
				} else {
					// No stones are added to the opponents score.
					stones++;
				}

				// Reset the pit index and change to opposite player pit for
				// next stone.
				isOppositePit = !isOppositePit;
				currentPlayer = getOpponentPlayer(currentPlayer);
				index = -1;
			}
		}
		if (nextMove.equals(GameMove.CAPTURE)) {
			capture(index);
		}
		return nextMove;
	}

	/**
	 * Change the current player in the Game.
	 *
	 * @return the GameState to decide the next player.
	 */
	GameState changePlayer() {
		if (gameState.equals(GameState.PLAYER1)) {
			return GameState.PLAYER2;
		} else if (gameState.equals(GameState.PLAYER2)) {
			return GameState.PLAYER1;
		}
		// This scenario will never happen as there are enough validations
		// before this function call.
		return GameState.ERROR;
	}

	/**
	 * Checks if the Game is over.
	 */
	boolean isGameOver() {
		int minScoreToFinish = stonesPerPit * player1.getPitsCount();
		if (player1.getScore() > minScoreToFinish || player2.getScore() > minScoreToFinish
				|| (player1.getScore() == minScoreToFinish && player2.getScore() == minScoreToFinish)) {
			return true;
		}
		if (!player1.hasStones()) {
			player2.addScore(player2.getTotalStonesInPit());
			return true;
		}
		if (!player2.hasStones()) {
			player1.addScore(player1.getTotalStonesInPit());
			return true;
		}
		return false;
	}

	/**
	 * Captures the last stone to be added to the player's empty pit and all the
	 * stones in the opposite pit. Adds all these stones to the player's score.
	 * 
	 * @param index
	 *            the index of the current player's pit.
	 */
	void capture(int index) {
		KalahPlayer currentPlayer = getCurrentPlayer();
		KalahPlayer opponentPlayer = getOpponentPlayer(currentPlayer);
		currentPlayer.addScore(currentPlayer.getPit(index).pickStones());
		currentPlayer.addScore(opponentPlayer.getPit((opponentPlayer.getPitsCount() - 1) - index).pickStones());
	}

	/**
	 * @param index
	 *            the index of the current player's pit.
	 * @return true if the pit is valid to pick stones, else false.
	 */
	boolean validPit(int index) {
		KalahPit pit = getCurrentPlayer().getPit(index);
		if (pit != null && !pit.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Gets the opponent {@link KalahPlayer} for the {@code currentPlayer}.
	 */
	KalahPlayer getOpponentPlayer(KalahPlayer currentPlayer) {
		if (currentPlayer.equals(player1)) {
			return player2;
		}
		return player1;
	}

	/**
	 * Gets the {@link KalahPlayer} who is playing the current move. 
	 */
	public KalahPlayer getCurrentPlayer() {
		KalahPlayer currentPlayer;
		if (gameState.equals(GameState.PLAYER1)) {
			currentPlayer = player1;
		} else if (gameState.equals(GameState.PLAYER2)) {
			currentPlayer = player2;
		} else {
			gameState = GameState.ERROR;
			return null;
		}
		return currentPlayer;
	}
}
