package com.backbase.kalah;

import java.util.ArrayList;

/** Class that defines one of the players for the Kalah Game. */
final class KalahPlayer {
	private final String name;
	private int score;
	private final ArrayList<KalahPit> pits;

	/**
	 * Constructor which will initialize a Player to have 6 pits with 6 stones
	 * each.
	 */
	KalahPlayer(String name) {
		this.name = name;
		this.score = 0;
		this.pits = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			KalahPit pit = new KalahPit();
			this.pits.add(pit);
		}
	}

	/**
	 * Constructor which allows change in the number of pits & number of stones
	 * in each pit.
	 * 
	 * @param name
	 *            the name of the player.
	 * @param pitCount
	 *            the number of pits for the player.
	 * @param stonePerPit
	 *            the number of stones in each pit.
	 */
	KalahPlayer(String name, int pitCount, int stonePerPit) {
		this.name = name;
		this.score = 0;
		this.pits = new ArrayList<>();
		for (int i = 0; i < pitCount; i++) {
			KalahPit pit = new KalahPit(stonePerPit);
			this.pits.add(pit);
		}
	}

	String getName() {
		return name;
	}

	int getScore() {
		return score;
	}

	/**
	 * Adds the stones to the Players Kalah / Score.
	 * 
	 * @param stones
	 *            Number of stones to be added.
	 */
	void addScore(int stones) {
		this.score += stones;
	}

	/**
	 * @param index
	 *            the index of the pit in {@code pits}.
	 * @return {@link KalahPit} from {@code pits}.
	 */
	KalahPit getPit(int index) {
		if (index < 0 || index >= pits.size()) {
			return null;
		}
		return pits.get(index);
	}

	ArrayList<KalahPit> getPits() {
		return pits;
	}

	/**
	 * Gets the total count of pits.
	 */
	int getPitsCount() {
		return pits.size();
	}

	/**
	 * Get the total number of stones in all pits.
	 */
	int getTotalStonesInPit() {
		int count = 0;
		for (KalahPit pit : pits) {
			count += pit.size();
		}
		return count;
	}

	/**
	 * Check if there are any stones in the pits.
	 */
	boolean hasStones() {
		return !(getTotalStonesInPit() == 0);
	}
}
