package com.backbase.kalah;

final class KalahPit {
	private int stoneCount;
	
	KalahPit() {
		this.stoneCount = 6;
	}
	
	KalahPit(int stoneCount) {
		this.stoneCount = stoneCount;
	}

	int pickStones() {
		int stones = this.stoneCount;
		this.stoneCount = 0;
		return stones;
	}
	
	int size() {
		return stoneCount;
	}
	
	boolean isEmpty() {
		return (this.stoneCount == 0);
	}
	
	void sowStone() {
		stoneCount++;
	}
}
