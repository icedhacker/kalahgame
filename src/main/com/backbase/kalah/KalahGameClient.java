package com.backbase.kalah;

import java.util.Scanner;

import com.backbase.kalah.KalahGame.GameState;

public class KalahGameClient {

	public static void main(String[] args) {
		System.out.println("Welcome to Kalah");
		Scanner in = new Scanner(System.in);

		// Set Player Names.
		String playerName1 = "Player 1";
		String playerName2 = "Player 2";

		// Initialize the Kalah Game and print initial board.
		KalahGame game = new KalahGame(playerName1, playerName2);
		printKalahBoard(game);

		// Start the game.
		game.startGame();
		while (true) {
			System.out.println(
					String.format("%s moves, Enter the pit index to pick : ", game.getCurrentPlayer().getName()));
			char pitIndex = in.next().charAt(0);
			game.playMove((int) pitIndex - 65);
			printKalahBoard(game);
			if (game.getGameState().equals(GameState.GAME_OVER)) {
				if (game.getWinner() != null) {
					System.out.println(String.format("Game Over. %s Wins !!", game.getWinner().getName()));
				} else {
					System.out.println("Game Over. Tied.");
				}
				break;
			}
		}
		in.close();
	}

	private static void printKalahBoard(KalahGame game) {
		System.out.println("Kalah Board");
		KalahPlayer player1 = game.getPlayer1();
		String player1PitsString = String.format("%d \t", player1.getScore());
		String player1IndexString = player1.getName();
		for (int i = player1.getPitsCount() - 1; i >= 0; i--) {
			KalahPit pit = player1.getPit(i);
			player1PitsString += "\t" + pit.size();
			player1IndexString += "\t" + (char) (65 + i);
		}

		KalahPlayer player2 = game.getPlayer2();
		String player2PitsString = "\t\t";
		String player2IndexString = "\t\t";
		for (int i = 0; i < player2.getPitsCount(); i++) {
			KalahPit pit = player2.getPit(i);
			player2PitsString += pit.size() + "\t";
			player2IndexString += (char) (65 + i) + "\t";
		}
		player2IndexString += player2.getName();
		player2PitsString += String.format("%d", player2.getScore());
		System.out.println(player1IndexString);
		System.out.println(player1PitsString);
		System.out.println();
		System.out.println(player2PitsString);
		System.out.println(player2IndexString);
	}

}
