package com.backbase.kalah;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class KalahPlayerTest {
	private static final String PLAYER_NAME = "Test Player";

	private KalahPlayer testPlayer;

	@Test
	public void testPlayerDefaultInitalization() {
		testPlayer = new KalahPlayer(PLAYER_NAME);
		assertTrue(testPlayer.getName().equals(PLAYER_NAME));
		assertTrue(testPlayer.getScore() == 0);
		assertTrue(testPlayer.getPitsCount() == 6);
		assertTrue(testPlayer.hasStones());
		assertTrue(testPlayer.getTotalStonesInPit() == 36);
	}

	@Test
	public void testPlayerDynamicInitalization() {
		testPlayer = new KalahPlayer(PLAYER_NAME, 6, 4);
		assertTrue(testPlayer.getName().equals(PLAYER_NAME));
		assertTrue(testPlayer.getScore() == 0);
		assertTrue(testPlayer.getPitsCount() == 6);
		assertTrue(testPlayer.hasStones());
		assertTrue(testPlayer.getTotalStonesInPit() == 24);
	}
}
