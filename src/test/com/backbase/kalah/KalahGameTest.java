package com.backbase.kalah;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.backbase.kalah.KalahGame.GameState;

public class KalahGameTest {
	private static final String PLAYER_1 = "Player 1";
	private static final String PLAYER_2 = "Player 2";
	private static final int STONES_IN_PIT = 6;
	private KalahGame kalahGame;

	@Before
	public void setup() {
		kalahGame = new KalahGame(PLAYER_1, PLAYER_2);
	}

	@Test
	public void testGameDefaultInitalization() {
		assertTrue(kalahGame.getGameState().equals(GameState.NOT_STARTED));
		assertTrue(kalahGame.getStonesPerPit() == STONES_IN_PIT);
	}

	@Test
	public void testGameStart() {
		kalahGame.startGame();
		assertTrue(kalahGame.getGameState().equals(GameState.PLAYER1));
	}

	@Test
	public void testGameStartError() {
		// Game state should be an error if the game is started when in any
		// state other than NOT_STARTED.
		kalahGame.startGame();
		kalahGame.startGame();
		assertTrue(kalahGame.getGameState().equals(GameState.ERROR));
	}

	@Test
	public void testPlayMovePlayAgain() {
		kalahGame.startGame();
		KalahPlayer currentPlayer = kalahGame.getCurrentPlayer();

		// Pick the stones from Player 1's pit index 0.
		kalahGame.playMove(0);
		assertTrue(currentPlayer.getScore() == 1);
		assertTrue(currentPlayer.getPit(0).isEmpty());
		assertTrue(kalahGame.getGameState().equals(GameState.PLAYER1));
	}

	@Test
	public void testPlayMoveChangePlayer() {
		kalahGame.startGame();
		KalahPlayer currentPlayer = kalahGame.getCurrentPlayer();

		// Pick the stones from Player 1's pit index 0.
		kalahGame.playMove(1);
		assertTrue(currentPlayer.getScore() == 1);
		assertTrue(kalahGame.getGameState().equals(GameState.PLAYER2));
	}

	@Test
	public void testPlayMoveIllegalMove() {
		kalahGame.startGame();

		// Pick the stones from Player 1's pit index 0.
		kalahGame.playMove(0);
		assertTrue(kalahGame.getGameState().equals(GameState.PLAYER1));

		kalahGame.playMove(0);
		assertTrue(kalahGame.getGameState().equals(GameState.PLAYER1));
	}

	@Test
	public void testPlayMoveCaptureMove() {
		kalahGame.startGame();
		KalahPlayer player1 = kalahGame.getPlayer1();

		// Player 1 moves.
		kalahGame.playMove(0);
		kalahGame.playMove(1);
		assertTrue(player1.getScore() == 2);

		// Player 2 moves.
		assertTrue(kalahGame.getGameState().equals(GameState.PLAYER2));
		KalahPlayer player2 = kalahGame.getPlayer2();
		kalahGame.playMove(0);
		assertTrue(player2.getScore() == 1);

		// Player 1 moves.
		assertTrue(kalahGame.getGameState().equals(GameState.PLAYER1));
		// Pit index 1 is empty.
		assertTrue(player1.getPit(1).isEmpty());
		// Pit index 0 has only 1 stone.
		assertTrue(player1.getPit(0).size() == 1);
		// Opposite pit to Pit index 1 i.e. Oppst Pit index 4 should have 8
		// stones.
		assertTrue(player2.getPit(4).size() == 7);
		kalahGame.playMove(0);
		assertTrue(player2.getPit(4).isEmpty());
		assertTrue(player1.getScore() == 10);
		assertTrue(kalahGame.getGameState().equals(GameState.PLAYER2));
	}
	
	@Test
	public void testGameFinish() {
		kalahGame.startGame();
		KalahPlayer player1 = kalahGame.getPlayer1();

		// Player 1 moves.
		kalahGame.playMove(0);
		kalahGame.playMove(1);
		assertTrue(player1.getScore() == 2);

		// Player 2 moves.
		KalahPlayer player2 = kalahGame.getPlayer2();
		kalahGame.playMove(0);
		assertTrue(player2.getScore() == 1);

		// Player 1 moves.
		kalahGame.playMove(0);
		assertTrue(player1.getScore() == 10);
		
		// Player 2 moves.
		kalahGame.playMove(1);
		assertTrue(player2.getScore() == 2);
		
		// Player 1 moves
		kalahGame.playMove(2);
		assertTrue(player1.getScore() == 11);
		
		// Player 2 moves.
		kalahGame.playMove(1);
		assertTrue(player2.getScore() == 2);
		
		// Player 1 moves
		kalahGame.playMove(1);
		assertTrue(player1.getScore() == 21);
		
		// Player 2 moves.
		kalahGame.playMove(5);
		assertTrue(player2.getScore() == 3);
		
		// Player 1 moves
		kalahGame.playMove(3);
		assertTrue(player1.getScore() == 22);
		
		// Player 2 moves.
		kalahGame.playMove(1);
		assertTrue(player2.getScore() == 3);
		
		// Player 1 moves
		kalahGame.playMove(2);
		assertTrue(player1.getScore() == 35);
		
		// Player 2 moves
		kalahGame.playMove(0);
		assertTrue(player2.getScore() == 3);
		
		// Player 1 moves
		kalahGame.playMove(1);
		assertTrue(player1.getScore() == 38);
		
		assertTrue(kalahGame.getGameState().equals(GameState.GAME_OVER));
		assertTrue(kalahGame.getWinner().equals(player1));
	}
}
